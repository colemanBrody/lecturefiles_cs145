public class point {
    private int x;
    private int y;
    private int z;

    public point(int startx, int starty, int startz){
        x = startx;
        y = starty;
        z = startz;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public int getZ() {
        return z;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
