import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class day4_review {
    public static void main(String[] args) throws IOException {
        //files();
        objects();
//        orderOfOperations();
    }

    public static void files() throws IOException {
        Scanner scan = new Scanner(System.in);
        FileWriter fw = new FileWriter(new File("hi_there.txt"), true);
        System.out.println("enter some numbers.");
        while(scan.hasNextInt()){
            int num = scan.nextInt();
            fw.write(num + "\n");
        }
        fw.close();
        System.out.println("Good bye.");
    }

    public static void objects(){
        point p = new point(5,12,68);
        System.out.println(p);
    }

    public static String orderOfOperations(){
        String step1 = 9 + " hello " + 7 / 5 + Math.pow(4, 10);
        String step2 = 9 + " hello " + 7 / 5 + 1048576;
        String step3 = "9 hello " + 1 + 1048576;
        String step4 = "9 hello 1" + 1048576;
        String step5 = "9 hello 11048576";
        return step5;
    }
}
