import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class day3_review {
    public static void main(String[] args) {
        System.out.println(Math.random());
        Random spin = new Random();
        System.out.println(10 + spin.nextInt(10));

        ArrayList<Double> ald = new ArrayList<Double>();
        for(int i = 0; i < 10; i++){
            ald.add((double)(i * 3));
        }
        System.out.println(ald);
        System.out.println(ald.get(4));
        ald.set(6, 983.44);
        System.out.println(ald);

        for(double num : ald){
            System.out.println(num);
        }
    }

    public static boolean zen(int a, int b){
        return a == b;
    }

    public static void asdfasdf(){
        stuff[] things = new stuff[5];
        System.out.println(Arrays.toString(things));

        things[3] = new stuff("Brodys hat?", 87);
        System.out.println(Arrays.toString(things));
    }

    public static void doMatrixSum(){
        int[][] matrix = {{1, 0, 1}, {3, 1, 6, 9, 3, 6, 2}, {2, 9, 7}};

        System.out.println(Arrays.toString(matrix[1]));
        System.out.println(matrix[1][1]);

        System.out.println(matrixSum(matrix));
    }

    public static int matrixSum(int[][] matrix){
        int matrixSum = 0;
        for(int[] row : matrix){
            for(int num : row){
                matrixSum += num;
            }
        }
        return matrixSum;
    }
    public static void scannersAndWhile(){
        Scanner s = new Scanner(System.in);
        int sum = 0;
        System.out.println("enter a number");
        while(s.hasNextInt()){
            sum += s.nextInt();
            while(!s.hasNextInt()){
                System.out.println("That is not an int!");
                s.next();
            }
            System.out.println("enter a number");
        }
        System.out.println(sum);
    }

    public static void equalsthings(){
        String data = "Class";
        String otherString = (new Scanner(System.in)).next();
        System.out.println(areTheyEqual(data, otherString));

        stuff firstStuff = new stuff("hello", 5);
        stuff secondStuff = new stuff("test", 5);
        System.out.println(firstStuff.equals(secondStuff));
        System.out.println(firstStuff);
    }

    public static boolean areTheyEqual(String s1, String s2){
        return s1.equals(s2);
    }
}

class stuff{
    String l;
    int n;

    public stuff(String s1, int num){
        l = s1;
        n = num;
    }

    public String toString(){
        return l;
    }

    public boolean equals(Object o){
        return n == ((stuff)o).n;
    }
}