/*
    Brody Coleman
    Whatcom Community College
    CS145
    10/01/2018

    Review of CS140 day 2.
*/

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

class thing{
    public int h = 0;
    public String desc = "tree";
}

public class day2_review {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        int[] nums = {4, 5, 9, 32, 7, -14, 888888888};
         int result = otherThing(average(nums));
        System.out.println(result);

        for(int i : nums){
            System.out.printf("Hi %7d is your number\n", i);
        }

        Math.tan(26.54);
        System.out.println(Math.tan(26.54));
    }

    public static void doOddJSONthings(){
        thing t =  new thing();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        System.out.println(gson.toJson(t));
    }

    public static int otherThing(int i){
        return i * 5;
    }
    public static int average(int[] numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum / numbers.length;
    }
}
