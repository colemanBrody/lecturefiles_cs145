import java.awt.*;
import java.util.*;

public class Main {
    /*
        Its the main.
     */
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        Scanner input = new Scanner(System.in);
        drawDisco(input);

//        System.out.println("How many words?");
//        int wordNumber = input.nextInt();
//        input.nextLine();
//
//        String[] words = new String[wordNumber];
//        //String[] words = new String[22];
//
//        for (int i = 0; i < wordNumber; i++) {
//            System.out.println("what is your word?");
//            words[i] = input.nextLine();
//        }
//
//        String vowels = "aeiou";
//        int vowelCount = 0;
//
//        for(String word : words){
//            for(char character : word.toCharArray()){
//                String chard = character + "";
//                if(vowels.toUpperCase().contains(chard) || vowels.contains(chard)){
//                    vowelCount++;
//                }
//            }
//        }
//
//        System.out.println("There were " + vowelCount + " vowels.");
    }

    public static void accumulateVowelSum(){
        String[] words = {"ice cream", "canvas", "Apparatus", "temperature", "a"};
        String vowels = "aeiou";
        int vowelCount = 0;

        for(String word : words){
            for(char character : word.toCharArray()){
                String chard = character + "";
                if(vowels.toUpperCase().contains(chard) || vowels.contains(chard)){
                    vowelCount++;
                }
            }
        }

        System.out.println("There were " + vowelCount + " vowels.");
    }
    public static void sumBatch(){
        sumMany(7, 6, 4, 3, 5, 7, -7, 2, 54, 7);
        sumMany();
        sumMagic(7, 6, 4, 3, 5, 7, -7, 2, 54, 7);
        sumMagic();

        int a = 7;
        sumThree(a,-9756, 100100);
    }

    public static void sumMany(int... numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        System.out.println(sum);
    }

    public static void sumThree(int a, int b, int c){
        a = 67;
        System.out.println(a + b + c);
    }

    public static void sumMagic(int... numbers) {
        System.out.println(Arrays.stream(numbers).sum());
    }

    public static void drawDisco(Scanner input){
        System.out.print("How wide?");
        int width = input.nextInt();
        System.out.print("How tall?");
        int height = input.nextInt();
        DrawingPanel draw = new DrawingPanel(width, height);
        Graphics g = draw.getGraphics();
        g.setColor(Color.CYAN);
        for(int x = 50; x < 1000; x++){
            for(int y = 50; y < 1000; y++){
                g.drawRect(50,100, (int)(Math.random() * 400),(int)(Math.random() * 400));
            }
            int red = (int)(Math.random() * 255);
            int green = (int)(Math.random() * 255);
            int blue = (int)(Math.random() * 255);
            g.setColor(new Color(red, green, blue));
        }
    }

    public static void ifs(){
        int[] ints = {5, 6, 7, 9, 3, 9, 800, 56, 343, 89568};
        //This is an if statement
        for (int i = 0; i < ints.length; i++) {
            int o = ints[i];
            if (o > 500) {
                System.out.println("its a big one!");
                if(o >1000){
                    System.out.println("super big!");
                }
                //break;
            } else if (o < 500) {
                System.out.println("Not so grand!");
            } else {
                System.out.println("Right on!");
            }
        }
    }
}
